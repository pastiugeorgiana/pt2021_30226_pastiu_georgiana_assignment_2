package View;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class View extends JFrame{
    private JTextField minServiceTime = new JTextField(10);
    private JTextField maxServiceTime = new JTextField(10);
    private JTextField maxArrivalTime = new JTextField(10);
    private JTextField minArrivalTime = new JTextField(10);
    private JTextField noOfServers = new JTextField(10);
    private JTextField noOfClients = new JTextField(10);
    private JTextField simulationTime = new JTextField(10);
    private JCheckBox strategyTime = new JCheckBox("Time strategy");
    private JCheckBox strategyLen = new JCheckBox("Length strategy");
    private JTextArea log = new JTextArea();
    private JFrame mainFrame = new JFrame();
    private JFrame setupFrame = new JFrame();
    private JButton start = new JButton("Start");
    private boolean started = false;
    private JPanel logger = new JPanel();
    private JLabel l1 = new JLabel("Simulation setup:");
    private JLabel l2 = new JLabel("Simulation Time:");
    private JLabel l3 = new JLabel("Number Of Queues:");
    private JLabel l = new JLabel("Minimum Arrival Time:");
    private JLabel l4 = new JLabel("Maximum Arrival Time:");
    private JLabel l5 = new JLabel("Minimum Service Time:");
    private JLabel l6 = new JLabel("Maximum Serivice Time:");
    private JLabel l7 = new JLabel("Number Of Clients:");
    public View() {
        setupFrame.setSize(300, 300);
        setupFrame.setLayout(new GridLayout(6,2));

        JPanel op1 = new JPanel();
        op1.add(l1);
        op1.setVisible(true);
        setupFrame.add(op1);
        setupFrame.add(new JPanel());
        JPanel op7 = new JPanel();
        op7.add(l7);
        op7.add(noOfClients);
        setupFrame.add(op7);

        JPanel op3 = new JPanel();
        op3.add(l3);
        op3.add(this.noOfServers);
        setupFrame.add(op3);
        JPanel op = new JPanel();
        op.add(l);
        op.add(minArrivalTime);
        setupFrame.add(op);
        JPanel op4 = new JPanel();
        op4.add(l4);
        op4.add(maxArrivalTime);
        setupFrame.add(op4);
        JPanel op5 = new JPanel();
        op5.add(l5);
        op5.add(minServiceTime);
        setupFrame.add(op5);
        JPanel op6 = new JPanel();
        op6.add(l6);
        op6.add(maxServiceTime);
        setupFrame.add(op6);
        JPanel op2 = new JPanel();
        op2.add(l2);
        op2.add(simulationTime);
        setupFrame.add(op2);
        JPanel op8 = new JPanel();
        op8.setLayout(new FlowLayout());
        op8.add(strategyTime);
        op8.add(strategyLen);
        setupFrame.add(op8);
        setupFrame.add(start);
        logger.setVisible(true);
        logger.setLayout(new BoxLayout(logger,BoxLayout.Y_AXIS));
        log.setEditable(false);

        JScrollPane scrollPan = new JScrollPane(this.log);
        scrollPan.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPan.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        System.out.println("Log of events");
        logger.add(new JLabel("Log of events"));
        logger.add(scrollPan);

        setupFrame.setSize(400, 500);
        mainFrame.setSize(400,1000);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setupFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setLayout(new GridLayout(1,3));
        mainFrame.add(logger);
        setupFrame.setVisible(true);
        mainFrame.setVisible(true);

    }

    public void addButtonListener(ActionListener btn) {
        start.addActionListener(btn);
    }

    public void addlog(String s) {
        log.append(s);
    }

    public String getMinServiceTime() {
        return minServiceTime.getText();
    }

    public String getMaxServiceTime() {
        return maxServiceTime.getText();
    }

    public String getMaxArrivalTime() {
        return maxArrivalTime.getText();
    }

    public String getNoOfServers() {
        return noOfServers.getText();
    }

    public String getNoOfClients() {
        return noOfClients.getText();
    }

    public String getSimulationTime() {
        return simulationTime.getText();
    }

    public boolean getStrategyTime() {
        return strategyTime.isSelected();
    }

    public boolean getStrategyLen() {
        return strategyLen.isSelected();
    }

    public String getMinArrivalTime() {
        return minArrivalTime.getText();
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }
}
