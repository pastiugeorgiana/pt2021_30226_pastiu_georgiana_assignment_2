package Model;

import View.View;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.Thread.*;


public class Queue implements Runnable {
    private final BlockingQueue<Client> client;
    private int wait;
     View frame;
    private final AtomicBoolean running = new AtomicBoolean(false);

    public Queue(View frame) {
        this.wait =0 ;
        this.client = new LinkedBlockingQueue<>(1000);
        this.frame = frame;
    }

    public void addClient(Client newClient) {
        client.add(newClient);
        newClient.setFinishTime(newClient.getFinishTime()+wait);
        wait+=newClient.getProcessingTime();
    }

    public void stop() {
        running.set(false);
    }

    public void run() {
        running.set(true);
        while(running.get()) {
            if(!client.isEmpty()) {
                Client c = client.element();
                try {
                    sleep(c.getProcessingTime()* 1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                client.remove();
                wait-=c.getProcessingTime();

            }

        }

    }

    public int serverLength() {
        int length = 0;

        for (Client ignored : client) {
            length++;
        }

        return length;
    }

    public BlockingQueue<Client> getClient() {
        return client;
    }

    public int getNumberOfClients() {
        int nr = 0;
        for (Client ignored : client) nr++;
        return nr;
    }

    public int getWaitingTime() {
        return wait;
    }


    public void setWaitingTime(int waitingTime) {
        this.wait = waitingTime;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for(Client c : client) {
            s.append("(").append(c.getID()).append(",").append(c.getArrivalTime()).append(",").append(c.getProcessingTime()).append(");");
        }
        if(s.toString().equals("")) {
            s.append("closed");
        }
        return s.toString();
    }

}
