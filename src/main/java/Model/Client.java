package Model;

public class Client implements Comparable {
    private final int arrivalTime;
    private final int processingTime;
    private int finishTime;
    private final int ID;


    public Client(int arrival, int processing,int ID) {
        this.arrivalTime = arrival;
        this.processingTime = processing;
        this.finishTime = arrival + processing;
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getProcessingTime() {
        return processingTime;
    }


    public int getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(int finishTime) {
        this.finishTime = finishTime;
    }

    public int compareTo(Object o) {
        Client c = (Client)o;
        return this.arrivalTime- c.getArrivalTime();
    }

}
