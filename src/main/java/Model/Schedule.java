package Model;

import View.View;

import java.util.ArrayList;


public class Schedule {
    private final ArrayList<Queue> queues;
    private Strategy strategy = new Strategy.StrategyTime();
    private final View frame;

    public Schedule(int maxServers, View frame) {
        queues = new ArrayList<>(maxServers);
        this.frame = frame;
        Thread t;
        for(int i = 0; i < maxServers; i++) {
            Queue serv = new Queue(frame);
            queues.add(serv);
            t = new Thread(serv);
            t.start();
        }
    }

    public void setStrategy(int selection) {
        if(selection == 0) {
            this.strategy = new Strategy.StrategyLength();
        }
        if(selection == 1) {
            this.strategy = new Strategy.StrategyTime();
        }
    }

    public void takeClient(Client c) {
        strategy.addTask(this.queues, c,frame);
    }

    public ArrayList<Queue> getServers() {
        return queues;
    }

    public String serversData() {
        StringBuilder s = new StringBuilder();
        int i = 0;
        for(Queue sv : queues) {
            i++;
            s.append("Queue ").append(i).append(":").append(sv.toString()).append("\n");
        }
        return s.toString();
    }

}
