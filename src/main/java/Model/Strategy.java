package Model;

import View.View;

import java.util.Iterator;
import java.util.List;

public interface Strategy {
     void addTask(List<Queue> queues, Client c, View frame);

    class StrategyLength implements Strategy {

        public void addTask(List<Queue> queues, Client c, View frame) {
            int length;
            int index = 0;
            int i = 0;
            length = queues.get(0).serverLength();
            for(Iterator<Queue> it = queues.iterator(); it.hasNext();){
                Queue serv = it.next();
                if(length > serv.serverLength()) {
                    length = serv.serverLength();
                    index = i;
                }
                i++;
            }

            queues.get(index).addClient(c);
        }

    }

    class StrategyTime implements Strategy {

        public void addTask(List<Queue> queues, Client c, View frame) {
            int time;
            int index = 0;
            int i = 0;
            time = queues.get(0).getWaitingTime();
            for(Iterator<Queue> it = queues.iterator(); it.hasNext();){

                Queue serv = it.next();
                if(time > serv.getWaitingTime()) {
                    time = serv.getWaitingTime();
                    index = i;
                }
                i++;
            }
            queues.get(index).addClient(c);
        }


    }
}
