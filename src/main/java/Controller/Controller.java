package Controller;

import Model.Client;
import Model.Schedule;
import Model.Queue;
import View.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public class Controller implements Runnable {
    public int timeLimit = 100;
    public int maxProcessingTime = 20;
    public int minProcessingTime = 0;
    public int nrOfQueues = 3;
    public int nrOfClients = 5;
    public int selection = 0;
    public int maxArrivalTime = 20;
    public int minArrivalTime = 0;
    private final View frame;
    private final ArrayList<Client> generatedClients;

    public Controller() {
        frame = new View();
        generatedClients = new ArrayList<>();
        frame.addButtonListener(new Controller.StartListener());
    }
    class StartListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            frame.setStarted(true);
            frame.repaint();
            frame.revalidate();
        }
    }

    public int getDataFromUI() {
        int i = 1;
        try {
            this.timeLimit = Integer.parseInt(frame.getSimulationTime());
            this.maxProcessingTime = Integer.parseInt(frame.getMaxServiceTime());
            this.minProcessingTime = Integer.parseInt(frame.getMinServiceTime());
            this.nrOfQueues = Integer.parseInt(frame.getNoOfServers());
            this.nrOfClients = Integer.parseInt(frame.getNoOfClients());
            this.maxArrivalTime = Integer.parseInt(frame.getMaxArrivalTime());
            this.minArrivalTime = Integer.parseInt(frame.getMinArrivalTime());
            boolean isstrategyLen = frame.getStrategyLen();
            boolean isstrategyTime = frame.getStrategyTime();
            if(isstrategyLen) {
                this.selection = 0;
            }
            if(isstrategyTime) {
                this.selection = 1;
            }
            if(timeLimit <= 0){frame.addlog("Time is null/negative!");
                System.out.println("Time is null/negative!");}
               if( maxProcessingTime < minProcessingTime){
                   frame.addlog("max Service Time < min Service Time!");
                   System.out.println("max Service Time < min Service Time!");}
                   if( nrOfQueues <= 0 || nrOfQueues > 20 || nrOfClients <= 0 || maxArrivalTime < minArrivalTime || minProcessingTime <= 0 || minArrivalTime < 0) {
                       { frame.addlog("Incorrect data parameters");
                       System.out.println("Incorrect data parameters");}
            }
        }catch(NumberFormatException e) {
            frame.addlog("Incorrect data input");
            i = 0;
        }

        return i;
    }

    private void generateRandomClients(int nrOfClients) {
        int i = 1;
        Random rand = new Random();
        while(i < nrOfClients) {
            int arrivalTime = (int) (Math.random()*maxArrivalTime);
            arrivalTime += minArrivalTime;
            int processingTime = rand.nextInt(maxProcessingTime);
            processingTime += minProcessingTime;

            Client c = new Client(arrivalTime,processingTime,i);
            generatedClients.add(c);

            i++;
        }
        Collections.sort(generatedClients);
    }



    public String emptyTime(int []empty) {
        StringBuilder s = new StringBuilder();
        for(int i = 0; i < empty.length; i++) {
            if(empty[i] < 0) {
                empty[i] = 0;
            }
            s.append("Queue ").append(i + 1).append(" empty time:").append(empty[i]).append("\n");
        }
        return s.toString();
    }

    public void run() {
        int edgeTime = 0;
        int max = 0;
        float averageWaitingTime = 0;
        boolean start;
        do {
            start = frame.isStarted();
            System.out.println(start);
        } while (!start);
        int confirm = getDataFromUI();
        int [] emptyTime = new int[nrOfQueues];
        if(confirm == 1) {
            generateRandomClients(nrOfClients);
            Schedule schedule = new Schedule(nrOfQueues, frame);
            schedule.setStrategy(selection);
            int currentTime = 0;

            while(currentTime < timeLimit) {
                int aux = 0;
                System.out.println("Time  " + currentTime + "\n");
                frame.addlog("Time  " + currentTime + "\n");
                if(!generatedClients.isEmpty()) {
                    for (Client c : generatedClients) {
                        if (c.getArrivalTime() == currentTime) {

                            schedule.takeClient(c);

                        }

                    }
                }

                int i = 0;
                for(Queue s : schedule.getServers()) {
                    aux += s.getNumberOfClients();
                    averageWaitingTime += s.getWaitingTime();
                    if(s.getClient().peek() == null)
                        emptyTime[i] += 1;
                    i++;
                }
                if(aux > max) {
                    edgeTime = currentTime;
                    max = aux;
                }
                frame.addlog(schedule.serversData());
                System.out.println(schedule.serversData());
                for(Queue s : schedule.getServers()) {
                    if(s.getWaitingTime() > 0) {
                        s.setWaitingTime(s.getWaitingTime()-1);
                    } else {
                        s.setWaitingTime(0);
                    }
                }
                System.out.println(currentTime);
                currentTime++;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(currentTime == timeLimit) {
                    for(Queue serv: schedule.getServers()) {
                        serv.stop();
                    }
                }
            }
            averageWaitingTime = averageWaitingTime  / (nrOfQueues *timeLimit);
            frame.addlog(new StringBuilder().append("Peak hour: ").append(edgeTime).append(" number of clients: ").append(max).append("\n").append("Average waiting time:").append(averageWaitingTime).append("\n").append(emptyTime(emptyTime)).toString());
            System.out.println(new StringBuilder().append("Peak hour: ").append(edgeTime).append(" number of clients: ").append(max).append("\n").append("Average waiting time:").append(averageWaitingTime).append("\n").append(emptyTime(emptyTime)).toString());

        }
    }

    public static void main(String[] args) {
        Controller manager = new Controller();
        Thread t = new Thread(manager);
        t.start();
    }

}
